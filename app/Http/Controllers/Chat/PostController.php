<?php

namespace App\Http\Controllers\Chat;

use App\Events\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        event(new Message($request->username, $request->message));

        return [];
    }
}
